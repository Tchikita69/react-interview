import { useEffect, useState } from 'react';
import { Message, User } from '../assets/types';

import Author from './author';
import { TextMessage, ImageMessage } from './msgcontent';
import CreatedAt from './createdat';

interface MessageProps {
    key: string,
    owner: string,
    user: User,
    users: User[],
    message: Message
}

const MessageComponent = (props: MessageProps) => {
    const { owner, user, users, message} = props
    const [treatedMessage, setTreadedMessage] = useState<{content: string, mention: boolean}[]>([])

    const tokenize = (txt: string) => {
        var result: { idxStart: number; idxEnd: number; mention: string; }[] = []
        var nextMention = 0
        var indexToStart = 0

        while (nextMention !== -1) {
            var mentionIndex = txt.indexOf("@", indexToStart)
            var nextMention = txt.indexOf("@", mentionIndex + 1)

            users.every((usr) => {
                var idx = txt.substring(0, nextMention === -1 ? txt.length : nextMention).indexOf("@" + usr.username, indexToStart)
                if (idx >= 0) {
                    let idxStart = idx
                    let idxEnd = idx + usr.username.length + 1;
                    let mention = usr.username
                    result.push({idxStart, idxEnd, mention})
                    indexToStart = nextMention
                    return false
                }
                return true
            })
        }
        return result
    }

    const findMentions = (msg: Message) => {
        const txt = msg.type === 'text' ? msg.content : msg.caption
        var mentions = tokenize(txt)
        var splitted : {content: string, mention: boolean}[] = []
        var start = 0

        mentions.forEach((mention) => {
            splitted.push({content: txt.substring(start, mention.idxStart), mention: false})
            splitted.push({content: txt.substring(mention.idxStart, mention.idxEnd), mention: true})
            start = mention.idxEnd
        })
        splitted.push({content: txt.substring(start), mention: false})
        setTreadedMessage([...splitted])
    }

    useEffect(() => {
        findMentions(message)
    }, [])

    return (
        <div className={'msg_' + owner}>
            <Author name={user.username}/>
            {message.type === 'text' ? 
                <TextMessage owner={owner} msgcontent={treatedMessage}/> :
                <ImageMessage owner={owner} url={message.url} msgcontent={treatedMessage}/>
            }
            <CreatedAt date={message.createdAt}/>
        </div>
    )
}

export default MessageComponent;