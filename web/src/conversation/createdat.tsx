interface Props {
    date: number
}

const CreatedAt = (props: Props) => {
    const formatDate = (date: number) => {
        var newdate = new Date(date);

        var dd = newdate.getDate();
        var mm = newdate.getMonth() + 1;
        var yyyy = newdate.getFullYear();
    
        var hh = newdate.getHours();
        var mi = newdate.getMinutes();
        var seconds = newdate.getSeconds()

        var dmy =   (dd > 9 ? '' : '0') + dd + '/' + 
                    (mm>9 ? '' : '0') + mm + '/' +
                    yyyy

        var hms =   (hh > 9 ? '' : '0') + hh + ':' + 
                    (mi > 9 ? '' : '0') + mi + ':' + 
                    (seconds > 9 ? '' : '0') + seconds
        return dmy + '-' + hms
    }
    const { date } = props

    return (
        <div style={{margin: 10}}>
            <p>{formatDate(date)}</p>
        </div>
    )
}

export default CreatedAt