interface MessageProps {
    owner: string,
    msgcontent: {content: string, mention: boolean}[]
}

interface ImageMessageProps extends MessageProps {
    url: string,
}

export const TextMessage = (props: MessageProps) => {
    const { owner, msgcontent } = props

    return (
        <div className={'bubble_' + owner}>
            <div style={{display: "flex"}}>
                {msgcontent.map((msg, index) => {
                    return msg.mention ? <a href="#" className='mention' key={index}>{msg.content}</a>
                                    : <p key={index}>{msg.content}</p>
                })}
            </div>
        </div>
    )
}

export const ImageMessage = (props: ImageMessageProps) => {
    const { owner, url, msgcontent} = props

    return (
        <div className={'bubble_' + owner}>
            <div className='imagecontent'>
                <img src={url}/>
                <div className='caption'>
                    <div style={{display: "flex"}}>
                        {msgcontent.map((msg, index) => {
                            return msg.mention ? <a href="#" className='mention' key={index}>{msg.content}</a>
                                    : <p key={index}>{msg.content}</p>                            
                        })}
                    </div>
                </div>
            </div>
        </div>
    )
}
