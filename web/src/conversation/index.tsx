import { Message, User } from '../assets/types';
import { useEffect, useRef } from 'react';
import MessageComponent  from './message';

interface Props {
  className: string;
  currentUserId: string;
  messages: Message[];
  users: User[];
}

const Conversation = (props: Props) => {
  const { className, currentUserId, messages, users } = props;
  const messagesEndRef = useRef<HTMLDivElement>(null)

  const scrollToBottom = () => {
    if (messagesEndRef.current)
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  }
  
  const findUser = (id: string) => {
    const index = users.findIndex((user) => {
      return user.id === id;
    })
    return users[index]
  }

  const sortedUsers = (users: User[]) => {
    users.sort((x, y) => {
      return y.username.length - x.username.length
    })
  }

  // Sort users dictionnary to find captions in longer names first
  var usersDictionnary = [...users]
  sortedUsers(usersDictionnary)
  
  useEffect(scrollToBottom, [messages]);
  
  return (
    <div className={className} >
      {messages.map((msg) => {
        return (
                  <MessageComponent
                              key={msg.id}
                              owner={msg.senderId === currentUserId ? 'self' : 'others'}
                              user={findUser(msg.senderId)}
                              users={usersDictionnary}
                              message={msg}/>
        )
      })}
      <div ref={messagesEndRef} />
    </div>
  )
};

export default Conversation;
