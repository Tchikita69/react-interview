interface Props {
    name: string
}

const Author = (props: Props) => {
    return (
        <div style={{margin: 10}}>
            <p style={{fontWeight: "bold"}}>{props.name}</p>
        </div>
    )
}

export default Author